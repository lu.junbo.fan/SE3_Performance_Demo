package napier.performance.exercise.benchmark;

import napier.performance.exercise.BenchmarkUtils;
import napier.performance.exercise.command.*;

public class SequentialRemoveBenchmark {

	public static void main(String[] args) {
		
		ArrayListSequentialRemoveCommand alsrc = new ArrayListSequentialRemoveCommand();
		System.out.println("ArrayList took " + BenchmarkUtils.timeTaken(1, alsrc)
			    + " milli-seconds.");
		
		LinkedListSequentialRemoveCommand llsrc = new LinkedListSequentialRemoveCommand();
		System.out.println("LinkedList took " + BenchmarkUtils.timeTaken(1, llsrc)
			    + " milli-seconds.");
		
	}

}
