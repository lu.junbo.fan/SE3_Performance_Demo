package napier.performance.exercise.benchmark;

import napier.performance.exercise.BenchmarkUtils;
import napier.performance.exercise.command.*;

public class EscapeAnalysisBenchmark {

	public static void main(String[] args) {
		EscapeAnalysisCommand eac = new EscapeAnalysisCommand();
		System.out.println("EscapeAnalysis took " + BenchmarkUtils.timeTaken(1, eac)
			    + " milli-seconds.");

		NoEscapeAnalysisCommand neac = new NoEscapeAnalysisCommand();
		System.out.println("NoEscapeAnalysis took " + BenchmarkUtils.timeTaken(1, neac)
			    + " milli-seconds.");
	}

}
