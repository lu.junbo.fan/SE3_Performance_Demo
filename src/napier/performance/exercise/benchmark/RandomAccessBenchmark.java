package napier.performance.exercise.benchmark;

import napier.performance.exercise.BenchmarkUtils;
import napier.performance.exercise.command.*;

public class RandomAccessBenchmark {

	public static void main(String[] args) {
		
		ArrayListRandomAccessCommand alrac = new ArrayListRandomAccessCommand();
		System.out.println("ArrayList took " + BenchmarkUtils.timeTaken(100000, alrac)
			    + " milli-seconds.");
		
		LinkedListRandomAccessCommand llrac = new LinkedListRandomAccessCommand();
		System.out.println("LinkedList took " + BenchmarkUtils.timeTaken(100000, llrac)
			    + " milli-seconds.");
		
	}

}
