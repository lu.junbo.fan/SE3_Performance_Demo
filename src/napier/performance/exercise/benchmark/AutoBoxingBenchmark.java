package napier.performance.exercise.benchmark;

import napier.performance.exercise.BenchmarkUtils;
import napier.performance.exercise.command.*;

public class AutoBoxingBenchmark {

	public static void main(String[] args) {
		
		MakeIntArrayCommand intc = new MakeIntArrayCommand();
		System.out.println("Ints took up " + BenchmarkUtils.ramTaken(1, intc)
		    + "MB of memory.");
		intc = null;

		MakeIntegerArrayCommand integerc = new MakeIntegerArrayCommand();
		System.out.println("Integers took up " + BenchmarkUtils.ramTaken(1, integerc)
		    + "MB of memory.");
		integerc = null;
		
	}

}
