package napier.performance.exercise.benchmark;

import napier.performance.exercise.BenchmarkUtils;
import napier.performance.exercise.command.*;

public class ExceptionHandlingBenchmark {

	public static void main(String[] args) {
		
		System.out.println("Regex took " 
			    + BenchmarkUtils.timeTaken(1000000, new RegexCommand())
			    + " milli-seconds.");

			System.out.println("Exception handling took " 
				    + BenchmarkUtils.timeTaken(1000000, new ExceptionCommand())
				    + " milli-seconds.");
	}

}
