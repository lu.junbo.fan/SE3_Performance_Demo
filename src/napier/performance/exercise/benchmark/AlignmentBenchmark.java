package napier.performance.exercise.benchmark;

import napier.performance.exercise.BenchmarkUtils;
import napier.performance.exercise.command.*;

public class AlignmentBenchmark {

	public static void main(String[] args) {
		
		BooleanDuplicationCommand bdc = new BooleanDuplicationCommand();
		System.out.println("Booleans took up " + BenchmarkUtils.ramTaken(1, bdc)
		    + "MB of memory.");
		bdc = null;

		CharDuplicationCommand cdc = new CharDuplicationCommand();
		System.out.println("Chars took up " + BenchmarkUtils.ramTaken(1, cdc)
			    + "MB of memory.");
		cdc = null;
		
		FloatDuplicationCommand fdc = new FloatDuplicationCommand();
		System.out.println("Floats took up " + BenchmarkUtils.ramTaken(1, fdc)
			    + "MB of memory.");
		fdc = null;
		
		DoubleDuplicationCommand ddc = new DoubleDuplicationCommand();
		System.out.println("Doubles took up " + BenchmarkUtils.ramTaken(1, ddc)
			    + "MB of memory.");
		ddc = null;
	}

}
