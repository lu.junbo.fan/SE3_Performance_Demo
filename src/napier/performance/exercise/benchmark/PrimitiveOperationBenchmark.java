package napier.performance.exercise.benchmark;

import napier.performance.exercise.BenchmarkUtils;
import napier.performance.exercise.command.*;

public class PrimitiveOperationBenchmark {

	public static void main(String[] args) {
		
		System.out.println("Multiplication took " 
		    + BenchmarkUtils.timeTaken(1000000000, new MultiplicationCommand())
		    + " milli-seconds.");

		System.out.println("Division took " 
			    + BenchmarkUtils.timeTaken(1000000000, new DivisionCommand())
			    + " milli-seconds.");		
	}

}
