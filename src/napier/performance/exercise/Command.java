package napier.performance.exercise;

public interface Command {
	public void execute();
}