package napier.performance.exercise;

public class BenchmarkUtils {

	private static final long MEGABYTE = 1024L * 1024L;

	public static long bytesToMegabytes(long bytes) {
		return bytes / MEGABYTE;
	}

	public static long timeTaken(long cycles, Command c) {
		long begin = System.currentTimeMillis();
		for(int i=0; i<cycles; i++)
			c.execute();
		return System.currentTimeMillis() - begin;
	}

	public static long ramTaken(long cycles, Command c) {
		for(int i=0; i<cycles; i++)
			c.execute();
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		return bytesToMegabytes(runtime.totalMemory() - runtime.freeMemory());
	}

}
