package napier.performance.exercise;

public class Vector3D {
	private float x, y, z;

    public Vector3D(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public Vector3D add(Vector3D v) {
        return new Vector3D(x + v.x, y + v.y, z + v.z);
    }

    public float dot(Vector3D v) {
        return x * v.x + y * v.y + z * v.z;
    }
}
