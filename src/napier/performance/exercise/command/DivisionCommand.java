package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class DivisionCommand implements Command {

	private float a = 3.1415926f;
	
	@Override
	public void execute() {
		a /= 10.0f;
	}
	
}
