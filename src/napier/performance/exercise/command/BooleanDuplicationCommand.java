package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class BooleanDuplicationCommand implements Command {

	private BooleanBox[] bools = new BooleanBox[1000000];
	
	@Override
	public void execute() {
		for(int i=0; i<bools.length; i++)
			bools[i] = new BooleanBox();
	}
	
}

class BooleanBox {
	public boolean bool1 = true;
	//public boolean bool2 = false;
}