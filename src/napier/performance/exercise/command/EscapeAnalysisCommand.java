package napier.performance.exercise.command;

import napier.performance.exercise.Command;
import napier.performance.exercise.Vector3D;

public class EscapeAnalysisCommand implements Command {

	private static final int PROBLEM_SIZE = 1000000000;
	
	@Override
	public void execute() {
		float result = 0;
        for (int i = 0; i < PROBLEM_SIZE; i++) {
            Vector3D v1 = new Vector3D(i, i, i);
            result += v1.add(v1).dot(v1);
        }
	}		
}
