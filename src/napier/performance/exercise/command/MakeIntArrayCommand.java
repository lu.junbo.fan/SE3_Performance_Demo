package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class MakeIntArrayCommand implements Command {

	private int[] ints = new int[1000000];
	
	@Override
	public void execute() {
		for(int i=0; i<ints.length; i++)
			ints[i] = i;
	}
	
}
