package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class CharDuplicationCommand implements Command {

	private CharBox[] chars = new CharBox[1000000];
	
	@Override
	public void execute() {
		for(int i=0; i<chars.length; i++)
			chars[i] = new CharBox();
	}
	
}

class CharBox {
	public char c1 = 'a';
	//public char c2 = 'b';
}