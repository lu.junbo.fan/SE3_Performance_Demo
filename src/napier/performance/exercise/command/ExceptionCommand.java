package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class ExceptionCommand implements Command {

	@Override
	public void execute() {
		isInteger("12345x");
	}
	
	private boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch(Exception e) {
			return false;
		}
	}
	
}
