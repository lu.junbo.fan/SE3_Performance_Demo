package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class MakeIntegerArrayCommand implements Command {

	private Integer[] ints = new Integer[1000000];
	
	@Override
	public void execute() {
		for(int i=0; i<ints.length; i++)
			ints[i] = new Integer(i);
			//ints[i] = i;
	}
	
}
