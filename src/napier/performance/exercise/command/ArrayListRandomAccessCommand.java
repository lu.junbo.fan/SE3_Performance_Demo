package napier.performance.exercise.command;

import java.util.ArrayList;
import java.util.Random;

import napier.performance.exercise.Command;

public class ArrayListRandomAccessCommand implements Command {

	private static final int SIZE = 100000;
	private ArrayList<Integer> arraylist = new ArrayList<Integer>();
	private Random rand = new Random();

	public ArrayListRandomAccessCommand() {
		for(int i=0; i<SIZE;i++)
			arraylist.add(i, i);
	}

	@Override
	public void execute() {
		arraylist.get(rand.nextInt(SIZE));
	}

}
