package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class DoubleDuplicationCommand implements Command {

	private DoubleBox[] doubles = new DoubleBox[1000000];
	
	@Override
	public void execute() {
		for(int i=0; i<doubles.length; i++)
			doubles[i] = new DoubleBox();
	}
	
}

class DoubleBox {
	public double d1 = 0.0;
}