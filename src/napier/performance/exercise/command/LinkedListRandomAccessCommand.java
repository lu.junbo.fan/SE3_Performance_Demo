package napier.performance.exercise.command;

import java.util.LinkedList;
import java.util.Random;

import napier.performance.exercise.Command;

public class LinkedListRandomAccessCommand implements Command {

	private static final int SIZE = 100000;
	private LinkedList<Integer> linkedlist = new LinkedList<Integer>();
	private Random rand = new Random();
	
	public LinkedListRandomAccessCommand() {
		for(int i=0; i<SIZE;i++)
			linkedlist.add(i, i);
	}
	@Override
	public void execute() {
		linkedlist.get(rand.nextInt(SIZE));
	}
	
}
