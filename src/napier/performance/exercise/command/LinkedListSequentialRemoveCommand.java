package napier.performance.exercise.command;

import java.util.LinkedList;

import napier.performance.exercise.Command;

public class LinkedListSequentialRemoveCommand implements Command {

	private static final int SIZE = 100000;
	private LinkedList<Integer> linkedlist = new LinkedList<Integer>();
	
	public LinkedListSequentialRemoveCommand() {
		for(int i=0; i<SIZE;i++)
			linkedlist.add(i, i);
	}
	
	@Override
	public void execute() {
		for(int i=0; i<SIZE;i++)
			linkedlist.remove(new Integer(i));
	}
	
}
