package napier.performance.exercise.command;

import java.util.ArrayList;

import napier.performance.exercise.Command;

public class ArrayListSequentialRemoveCommand implements Command {

	private static final int SIZE = 100000;
	private ArrayList<Integer> arraylist = new ArrayList<Integer>();
	
	public ArrayListSequentialRemoveCommand() {
		for(int i=0; i<SIZE;i++)
			arraylist.add(i, i);
	}
	
	@Override
	public void execute() {
		for(int i=0; i<SIZE;i++)
			arraylist.remove(new Integer(i));
	}
	
}
