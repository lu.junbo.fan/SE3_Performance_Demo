package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class MultiplicationCommand implements Command {

	private float a = 3.1415926f;
	
	@Override
	public void execute() {		
		a *= 0.1f;
	}
	
}
