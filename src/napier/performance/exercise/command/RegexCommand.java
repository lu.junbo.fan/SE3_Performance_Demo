package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class RegexCommand implements Command {

	@Override
	public void execute() {
		isInteger("12345x");
	}
	
	private boolean isInteger(String s) {
		if(s.matches("\\d+"))
			return true;
		return false;
	}
}
