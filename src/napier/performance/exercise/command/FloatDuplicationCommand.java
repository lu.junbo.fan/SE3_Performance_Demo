package napier.performance.exercise.command;

import napier.performance.exercise.Command;

public class FloatDuplicationCommand implements Command {

	private FloatBox[] floats = new FloatBox[1000000];
	
	@Override
	public void execute() {
		for(int i=0; i<floats.length; i++)
			floats[i] = new FloatBox();
	}
	
}

class FloatBox {
	public float f1 = 0f;
	//public float f2 = 0f;
}